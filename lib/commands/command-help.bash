# -*- sh -*-

cari_help() {
  cat "$(cari_dir)/help.txt" | less
}

cari_extension_cmds() {
  local plugins_path plugin_path ext_cmd_path ext_cmds plugin
  plugins_path="$(get_plugin_path)"
  for plugin_path in "$plugins_path"/*; do
    plugin="$(basename "$plugin_path")"
    ext_cmd_path="$plugin_path/lib/commands"
    ext_cmds="$(find "$ext_cmd_path" -name "command*.bash" 2>/dev/null)"
    if [[ -n $ext_cmds ]]; then
      printf "\\nPLUGIN %s\\n" "$plugin"
      for ext_cmd in $ext_cmds; do
        ext_cmd_name="$(basename "$ext_cmd")"
        sed "s/-/ /g;s/.bash//;s/command-*/  cari $plugin/;" <<<"$ext_cmd_name"
      done | sort
    fi
  done
}

help_command() {
  local plugin_name="$1"
  local tool_version="$2"
  local plugin_path

  # If plugin name is present as first argument output plugin help info
  if [ -n "$plugin_name" ]; then
    plugin_path=$(get_plugin_path "$plugin_name")

    if [ -d "$plugin_path" ]; then
      if [ -f "${plugin_path}/README.md" ]; then
        print_plugin_help "$plugin_path"
      else
        printf "No documentation for plugin %s\\n" "$plugin_name" >&2
        exit 1
      fi
    else
      printf "No plugin named %s\\n" "$plugin_name" >&2
      exit 1
    fi
  else
    cari_help
    cari_extension_cmds
  fi
}

print_plugin_help() {
  local plugin_path=$1
  if [ -f "${plugin_path}"/README.md ]; then
    markdown "${plugin_path}"/README.md > "${plugin_path}"/README.html
    lynx -dump "${plugin_path}"/README.html
  fi
}

help_command "$@"
