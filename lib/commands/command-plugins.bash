# -*- sh -*-

plugins_command() {

  if [[ $1 == *"--added"* ]]; then
#    echo $1
    plugins_list
    exit 0
  fi
  
  # All remote default plugins..
  initialize_or_update_repository

  local plugins_index_path
  plugins_index_path="$(cari_data_dir)/repository/plugins"

  local plugins_local_path
  plugins_local_path="$(get_plugin_path)"

  if ls "$plugins_index_path" &>/dev/null; then
    (
      for index_plugin in "$plugins_index_path"/*; do
        index_plugin_name=$(basename "$index_plugin")
        source_url=$(get_plugin_source_url "$index_plugin_name")
        added_flag=" "

        [[ -d "${plugins_local_path}/${index_plugin_name}" ]] && added_flag='*'

        printf "%s\\t%s\\n" "$index_plugin_name" "$added_flag$source_url"
      done
    ) | awk '{ printf("%-28s", $1); sub(/^[^*]/, " &", $2); $1=""; print $0 }'
  else
    printf "%s%s\\n" "error: index of plugins not found at " "$plugins_index_path"
  fi
}

# Installed only..
plugins_list() {
  local plugins_path
  plugins_path=$(get_plugin_path)

  if ls "$plugins_path" &>/dev/null; then
    (
      for plugin_path in "$plugins_path"/*; do
        plugin_name=$(basename "$plugin_path")
        printf "%s" "$plugin_name"

        printf "\\t%s" "$(git --git-dir "$plugin_path/.git" remote get-url origin 2>/dev/null)"

        local branch
        local gitref
        branch=$(git --git-dir "$plugin_path/.git" rev-parse --abbrev-ref HEAD 2>/dev/null)
        gitref=$(git --git-dir "$plugin_path/.git" rev-parse --short HEAD 2>/dev/null)
        printf "\\t%s\\t%s" "$branch" "$gitref"

        printf "\\n"
      done
    ) | awk '{ if (NF > 1) { printf("%-28s", $1) ; $1="" }; print $0}'
  else
    display_error 'No plugins added'
    exit 1
  fi
}

plugins_command "$@"
