# -*- sh -*-

handle_failure() {
  local install_path="$1"
  rm -rf "$install_path"
  exit 1
}

handle_cancel() {
  local install_path="$1"
  printf "\\nreceived sigint, cleaning up"
  handle_failure "$install_path"
}

install_command() {
  local plugin_name=$1
  local full_version=$2
  local extra_args="${*:3}"
	
  if [ "$plugin_name" = "" ] && [ "$full_version" = "" ]; then
    printf "No package name [<version>] specified..\\n"
  elif [[ $# -eq 1 ]]; then
    install_tool_version "$plugin_name" "latest"
  else
    install_tool_version "$plugin_name" "$full_version" "$extra_args"
  fi
}

get_concurrency() {
  if command -v nproc >/dev/null 2>&1; then
    nproc
  elif command -v sysctl >/dev/null 2>&1 && sysctl hw.ncpu >/dev/null 2>&1; then
    sysctl -n hw.ncpu
  elif [ -f /proc/cpuinfo ]; then
    grep -c processor /proc/cpuinfo
  else
    printf "1\\n"
  fi
}

install_one_local_tool() {
  local plugin_name=$1
  local search_path
  search_path=$(pwd)

  local plugin_versions

  local plugin_version

  local plugin_version_and_path
  plugin_version_and_path="$(find_versions "$plugin_name" "$search_path")"

  if [ -n "$plugin_version_and_path" ]; then
    local plugin_version
    some_tools_installed='yes'
    plugin_versions=$(cut -d '|' -f 1 <<<"$plugin_version_and_path")
    for plugin_version in $plugin_versions; do
      install_tool_version "$plugin_name" "$plugin_version"
    done
  else
    printf "No versions specified for %s in config files or environment\\n" "$plugin_name"
    exit 1
  fi
}

install_tool_version() {
  local plugin_name=$1
  local full_version=$2
  local flags=$3
  local keep_download
  local plugin_path

  plugin_path=$(get_plugin_path "$plugin_name")
  check_if_plugin_exists "$plugin_name"

  for flag in $flags; do
    case "$flag" in
    "--keep-download")
      keep_download=true
      shift
      ;;
    *)
      shift
      ;;
    esac
  done

  if [ "$full_version" = "system" ]; then
    return
  fi

  IFS=':' read -r -a version_info <<<"$full_version"
  if [ "${version_info[0]}" = "ref" ]; then
    local install_type="${version_info[0]}"
    local version="${version_info[1]}"
  else
    local install_type="version"

    if [ "${version_info[0]}" = "latest" ]; then
      local version
      version=$(cari latest "$plugin_name" "${version_info[1]}")
      full_version=$version
    else
      local version="${version_info[0]}"
    fi
  fi

  local install_path
  install_path=$(get_install_path "$plugin_name" "$install_type" "$version")
  local download_path
  download_path=$(get_download_path "$plugin_name" "$install_type" "$version")
  venv_path=$(get_venv_path)
  export CARI_VENV_PATH=$venv_path
  local concurrency
  concurrency=$(get_concurrency)
  trap 'handle_cancel $install_path' INT

  if [ -d "$install_path" ]; then
    printf "%s %s is already installed\\n" "$plugin_name" "$full_version"
  else

    if [ -f "${plugin_path}/bin/download" ]; then
      # Not a legacy plugin
      # Run the download script
      (
        # shellcheck disable=SC2030
        export CARI_INSTALL_TYPE=$install_type
        # shellcheck disable=SC2030
        export CARI_INSTALL_VERSION=$version
        # shellcheck disable=SC2030
        export CARI_INSTALL_PATH=$install_path
        # shellcheck disable=SC2030
        export CARI_DOWNLOAD_PATH=$download_path
        mkdir "$download_path"
        cari_run_hook "pre_cari_download_${plugin_name}" "$full_version"
        "${plugin_path}"/bin/download
      )
    fi

    local download_exit_code=$?
    if [ $download_exit_code -eq 0 ]; then
      (
        # shellcheck disable=SC2031
        export CARI_INSTALL_TYPE=$install_type
        # shellcheck disable=SC2031
        export CARI_INSTALL_VERSION=$version
        # shellcheck disable=SC2031
        export CARI_INSTALL_PATH=$install_path
        # shellcheck disable=SC2031
        export CARI_DOWNLOAD_PATH=$download_path
        # shellcheck disable=SC2031
        export CARI_CONCURRENCY=$concurrency
        mkdir "$install_path"
        cari_run_hook "pre_cari_install_${plugin_name}" "$full_version"
        "${plugin_path}"/bin/install
      )
    fi

    local install_exit_code=$?
    if [ $install_exit_code -eq 0 ] && [ $download_exit_code -eq 0 ]; then
      # Remove download directory if --keep-download flag or always_keep_download config setting are not set
      always_keep_download=$(get_cari_config_value "always_keep_download")
      if [ ! "$keep_download" = "true" ] && [ ! "$always_keep_download" = "yes" ] && [ -d "$download_path" ]; then
        rm -r "$download_path"
      fi

      cari reshim "$plugin_name" "$full_version"

      cari_run_hook "post_cari_install_${plugin_name}" "$full_version"
    else
      handle_failure "$install_path"
    fi
  fi
}

install_command "$@"
