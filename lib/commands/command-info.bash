# -*- sh -*-

info_command() {
  printf "%s\\n"		"--------------------------------------------------------------------------------"
  printf "%s:%s\\n" 	"NOTICE" "			Carinthia|Cari|cari is free software; it comes with no"
  printf "%s\\n" 				 "			guarantees or liability."
  printf "%s:%s\\n" 	"OS" "			$(uname -o) $(uname -r)"
  printf "%s:%s\\n" 	"CARI VERSION" "		$(cari_version)"
  printf "%s:%s\\n" 	"CARI EXECUTABLE" "	$(which cari)"
  printf "%s:%s\\n" 	"CARI ENVIRONMENT" "	$(env | grep -E "CARI_DIR|CARI_DATA_DIR|CARI_CONFIG_FILE|CARI_DEFAULT_TOOL_VERSIONS_FILENAME")"
  printf "%s:\\n" 		"INSTALLED PLUGINS"
  printf "%s\\n" 	 	"$(cari plugins --installed)"
}

info_command "$@"
