# -*- sh -*-

shim_versions_command() {
  local shim_name=$1
  
  if [ -z $shim_name ]; then
  	 printf "Argument required: <name>\\n"
  	 exit 0
  fi
  
  shim_plugin_versions "$shim_name"
}

shim_versions_command "$@"
