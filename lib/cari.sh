# The cari function is a wrapper so we can export variables
cari() {
  local command
  command="$1"
  if [ "$#" -gt 0 ]; then
    shift
  fi

  case "$command" in
  "shell")
    # Commands that need to export variables
    eval "$(cari export-shell-version sh "$@")" 
    ;;
  *)
    # Forward other commands to cari script
    command cari "$command" "$@"
    ;;

  esac
}
