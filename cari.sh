# For Korn shells (ksh, mksh, etc.), capture $_ (the final parameter passed to
# the last command) straightaway, as it will contain the path to this script.
# For Bash, ${BASH_SOURCE[0]} will be used to obtain this script's path.
# For Zsh and others, $0 (the path to the shell or script) will be used.
_under="$_"
if [ -z "$CARI_DIR" ]; then
  if [ -n "${BASH_SOURCE[0]}" ]; then
    current_script_path="${BASH_SOURCE[0]}"
  elif [[ "$_under" == *".sh" ]]; then
    current_script_path="$_under"
  else
    current_script_path="$0"
  fi

  CARI_DIR="$(dirname "$current_script_path")"
fi
export CARI_DIR
# shellcheck disable=SC2016
[ -d "$CARI_DIR" ] || printf '$CARI_DIR is not a directory'

# Add cari to PATH
#
# if in $PATH, remove, regardless of if it is in the right place (at the front) or not.
# replace all occurrences - ${parameter//pattern/string}
CARI_BIN="${CARI_DIR}/bin"
CARI_USER_SHIMS="${CARI_DATA_DIR:-$HOME/.cari}/shims"
[[ ":$PATH:" == *":${CARI_BIN}:"* ]] && PATH="${PATH//$CARI_BIN:/}"
[[ ":$PATH:" == *":${CARI_USER_SHIMS}:"* ]] && PATH="${PATH//$CARI_USER_SHIMS:/}"
# add to front of $PATH
PATH="${CARI_BIN}:$PATH"
PATH="${CARI_USER_SHIMS}:$PATH"

# shellcheck source=lib/cari.sh
# Load the cari wrapper function
. "${CARI_DIR}/lib/cari.sh"
. "${CARI_DIR}/completions/cari.bash"

unset _under current_script_path CARI_BIN CARI_USER_SHIMS
