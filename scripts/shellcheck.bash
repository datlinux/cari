#!/usr/bin/env bash

exec shellcheck -s bash -x \
  cari.sh \
  completions/*.bash \
  bin/cari \
  bin/private/cari-exec \
  lib/utils.bash \
  lib/commands/*.bash \
  scripts/*.bash \
  test/test_helpers.bash \
  test/fixtures/dummy_plugin/bin/*
